package com.home;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Andrei-hp on 29.09.2016.
 */
public class ManyNumbers {
    public static void main(String[] args) throws IOException {
        BufferedReader st = new BufferedReader(new InputStreamReader(System.in));
     int a = Integer.parseInt(st.readLine());
        int b =Integer.parseInt(st.readLine());
        int c =Integer.parseInt(st.readLine());
        if (a < b && a < c) {
            System.out.println(a);
        } else if (b < a && b < c) {
            System.out.println(c);
        } else {
            System.out.println(c);
        }

      factorialWhile();
        factorialDoWhile();
          massiv10();
        massivNo5();
        Calculator();
        MinMaxMassiv();
        MinMaxMassiv2();
        SumMassiv();
        Calendar();
    }


    public static void factorialWhile() throws IOException {
        BufferedReader st = new BufferedReader(new InputStreamReader(System.in));
        int a =Integer.parseInt(st.readLine());
        System.out.println("Введите число! От 1-10");
        if(a < 0){
            System.out.println("Вы ввели отрицательный факториал! ");
        }else {
            while (a < 100){
                int b = 1;
                b = b * a;
                a++;
                System.out.println(b);
            }
        }
    }
    public static void factorialDoWhile() throws IOException {
        int b = 1;
        //  int a;
        do {
            BufferedReader st = new BufferedReader(new InputStreamReader(System.in));
            int a = Integer.parseInt(st.readLine());
            System.out.println("Введите число! От 1-10");
            //System.out.println("Вы ввели отрицательный факториал! ");
            if(a < 0) {
                System.out.println("Вы ввели отрицательный факториал!");
            } else {int result = 1;
                for (int i = 2; i <= a; i++)
                    result *= i;
                System.out.println(result);
                if (a == 0 || a == 1)
                    System.out.println(result);
            }
        } while (b < 0);{System.out.println("Конец");}
    }
    public static void massiv10() throws IOException {
        int[] arr = new int[20];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = i + 1;
            if (i >= arr.length / 2) {

            } else {
                System.out.println((arr[i]));
            }


        }

    }

    public static void massivNo5() {
        int[] arr = {2, 33, 5, 67, 54, 5, 22, 5, 78, 16};
        for (int i = 0; i < arr.length; i++) {
            for (int element : arr) {
                if (element == 5) {
                    continue;
                }
                System.out.println(element);

            }
        }
    }
    public static void Calculator() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String rt = reader.readLine();
        int a = 13;
        int b = 3;
        if (rt.equals("*")) {
            System.out.println(a * b);
        }
        if (rt.equals("/")) {
            System.out.println(a / b);
        }
        if (rt.equals("+")) {
            System.out.println(a + b);
        }
        if (rt.equals("-")) {
            System.out.println(a - b);
        }
    }

    public static void MinMaxMassiv() {
        int[] rt = {78, 5, 74, 95, 23, 498, 1, 45, 5, 78, 56, 52, 20};
        int max = rt[0], min = rt[0];
        for (int i = 0; i < rt.length; i++) {
            if (max < rt[i])
                max = rt[i];
            if (min > rt[i])
                min = rt[i];
            System.out.println("Min number = " + min);
            System.out.println("Max number = " + max);


        }
    }


    public static void MinMaxMassiv2() {
        int[][] rt = {{71, 45, 33, 67, 24, 85, 97, 98},
                {45, 67, 87, 34, 23, 12, 34, 521}};
        int min = rt[0][0];
        int max = rt[0][0];
        for (int i = 0; i < rt.length; i++) {
            for (int j = 0; j < 8; j++) {
                if (min > rt[i][j]) {
                    min = rt[i][j];
                }
                if (max < rt[i][j]) {
                    max = rt[i][j];
                }
            }
        }
        System.out.println("Min number = " + min);
        System.out.println("Max number = " + max);
    }


    public static void SumMassiv() {
        int[][] sm = {{9, 5, 6}, {23, 4, 6}, {12, 43, 7}};
        int Sum = 0;
        for (int i = 1; i < sm.length; i++) {
            for (int j = 0; j < 3; j++) {
                Sum = Sum + sm[i][j];

            }
        }
        System.out.println(Sum);
    }

    public static void Calendar() {
        int d = 1;
        int a = 5;
        int[][] c = new int[6][7];
        for (int i = 1; i < c.length; i++) {
            for (int j = 0; j < 7; j++) {

                c[i][j] = d;
                if (d == 32) {
                    break;
                }
                d++;
            }

        }
        while (a < 32) {
            System.out.print(a + ", ");
            a += 7;
        }
        System.out.println(" ");
        System.out.println("31 day - Wednesday");
    }
}

